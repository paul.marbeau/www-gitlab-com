---
layout: markdown_page
title: "Why Premium?"
description: "GitLab Premium is ideal for scaling organizations and for multi team usage. Find more information on the benefits here!"
canonical_path: "/pricing/premium/"
---

## GitLab Premium

GitLab Premium is ideal for scaling organizations and for multi team usage. In addition to the capabilities in Free, GitLab Premium adds enterprise level support with priority support, live upgrade assistance and a technical account manager and enterprise readiness features like High Availability, Disaster Recovery. It helps manage Compliance, improves developer productivity with productivity analytics, Group and File templates as well as streamlines project planning with roadmaps, single level epics, issue boards and issue analytics. GitLab premium helps you deploy with confidence with Operations Dashboards, Protected environments, Load performance testing and multi project pipeline visualizations.

Please note this is not a comprehensive set of capabilities in GitLab Premium, visit [about.gitlab.com/features](https://about.gitlab.com/features) for the latest. GitLab continuously adds features every month and evaluates features that can be moved to lower tiers to benefit more users.

### Increase Operational Efficiencies
To cater to scaling enterprises, Premium introduces capabilities that allow enterprises analyze team, project and group trends to uncover patterns and setup consistent standards to improve overall productivity.
> **90% of Paessler's QA is self served - the QA engineer’s tasks have been slashed from about an hour a day to 30 seconds, a 120x speed increase** <br><br> Every branch gets tested, it’s built into the pipeline. As soon as you commit your code, the whole process kicks off to get it tested. The amount of effort involved in actually getting to the newest version that you’re supposed to be testing, whether you’re a developer or a QA engineer, is minimized immensely. <br> ***Greg Campion*** <br> Senior Systems Administrator, Paessler <br> [Read more](/customers/paessler/)

### Deliver Better Products Faster
Premium supports enterprise grade high availability, disaster recovery and replication for geographically distributed organizations, thereby delivering improved uptime and performance.

> **Goldman Sachs improves from 1 build every two weeks to over a thousand per day** <br><br> GitLab has allowed us to dramatically increase the velocity of development in our Engineering Division. We believe GitLab’s dedication to helping enterprises rapidly and effectively bring software to market will help other companies achieve the same sort of efficiencies we have seen inside Goldman Sachs. We now see some teams running and merging 1000+ CI feature branch builds a day! <br> ***Andrew Knight*** <br> Managing Director, Goldman Sachs <br> [Read more](/customers/goldman-sachs/)

### Reduce Compliance Risk
Premium supports features like Instance Events (self-managed only), verified committers, signed commits which allow you to enforce and track compliance policies in your organization.

**Read all case studies [here](/customers/)**

# Premium Specific Features

The below list of features are after factoring in the announcement regarding [18 GitLab features moving to core](https://about.gitlab.com/blog/2020/03/30/new-features-to-core/). The timelines of the actual move of features to core in the product will be as per the linked issues in the announcement.

#### Implement more controls

| Securely control key actions allowing only selected profiles to perform them | ![Merge Request approval rules](https://about.gitlab.com/images/12_7/mr_approvals_by_code_owners_v12_7.png) |

| **Features** | **Value** |
|:---------|:------|
| [Multiple approvers](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/rules.html#eligible-approvers) | Multiple and selective sign off of merge request to speed up release in control   |
| [Merge approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/) | A set of approvers can provide more trust to the eventual result of a merge being confirmed |
| [Code owners](https://docs.gitlab.com/ee/user/project/code_owners.html) | Select anyone from the team – not necessarily a developer – to have approval permissions and broaden the projects reach  |
| [Protected Environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html) | Establish controls and limit the access to change specific environments |
| [Push rules, restricted push & MRs](https://docs.gitlab.com/ee/push_rules/push_rules.html#push-rules) | Automate and concatenate actions based on the commit messages of the pushes to repos  |

#### Improve Productivity

| Ensure your development teams are always able to be productive.  Streamline login, avoid downtime, minimize outages and reduce latency between distributed teams.  | ![Productivity Analytics](https://about.gitlab.com/images/12_3/productivity_analytics.png)  |
| Understand team contribution and identify opportunities | ![Group analytics](https://docs.gitlab.com/ee/user/group/contribution_analytics/img/group_stats_graph.png) |
| Feedback in context and in the right Merge Request | ![In context feedback](https://docs.gitlab.com/ee/ci/review_apps/img/toolbar_feedback_form_v13_5.png) |


| **Features** |  **Value** |
| [Productivity Analytics](https://docs.gitlab.com/ee/user/analytics/productivity_analytics.html) | Analyze graphs and reports to understand team, project, and group productivity for uncovering patterns and best practices to improve overall productivity.   |
| [Contribution analytics](https://docs.gitlab.com/ee/user/group/contribution_analytics/) | Understand and assess the different operational gaps your projects may run into  |
| [Customizable Value Stream Analytics](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html#customizable-value-stream-analytics) | Create multiple value streams, hide default stages and create custom stages that align better with your development workflow |
| [Repository Analytics](https://docs.gitlab.com/ee/user/group/repositories_analytics/) | Visualize code coverage for select or all projects in your group |
| [Group and File Templates](https://docs.gitlab.com/ee/user/group/#group-file-templates) | Establish consistent and standard practices |
| [Code quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html) | Measure the impact of development practices and improve them faster  |
| [Visual reviews](https://docs.gitlab.com/ee/ci/review_apps/#visual-reviews) | Let reviewers provide feedback in the live environment where changes have been applied: reliable and faster feedback loops |
| [Advanced search](https://docs.gitlab.com/ee/user/search/advanced_global_search.html) | Nail down resource and component search to promote inner sourcing |
|[Consolidate alerts from different IT alerting tools](https://docs.gitlab.com/ee/operations/incident_management/integrations.html#http-endpoints) | It’s difficult to manage integrations between tools, especially when several monitoring tools like Nagios, Solarwinds, etc. alert on your services. These integrations notify you and your team of incidents, so it’s critical for them to be easy to set up and maintain. |


#### Streamline Project Planning

|  Manage [multiple agile projects (programs)](https://about.gitlab.com/solutions/agile-delivery/) with intuitive and easy to use dashboards and reports to track issues and milestones across multiple projects.  |  ![Assignee Lists](https://docs.gitlab.com/ee/user/project/img/issue_board_assignee_lists_v13_6.png)  |
| Distribute different tasks to different team members and let them work together to deliver results  | ![Multiple assignees](https://docs.gitlab.com/ee/user/project/issues/img/multiple_assignees_for_issues.png) |


| **Features**    | **Value** |
| --------- | ------------ |
| [Roadmaps](https://docs.gitlab.com/ee/user/group/roadmap/) | Visualize the flow of business initiatives across time in order to plan when future features will ship.   |
| [Single Level Epics](https://docs.gitlab.com/ee/user/group/epics/) | Manage your portfolio of projects more efficiently by tracking groups of issues that share a theme, across projects and milestones.   |
| [Confidential Epics](https://docs.gitlab.com/ee/user/group/epics/manage_epics.html#make-an-epic-confidential) | Manage private information (issues and sub epics) via confidential epics |
| [Group Backlog management](https://docs.gitlab.com/ee/user/project/issue_board.html#multiple-issue-boards) | Simplify tracking, scoping and planning future work with group level backlog management on multiple issue boards.   |
| [Group Milestone Boards/Lists](https://docs.gitlab.com/ee/user/project/issue_board.html#multiple-issue-boards) | Visualize  future work to be delivered in future releases/milestones.   |
| [Group Wikis](https://docs.gitlab.com/ee/user/group/index.html#group-wikis) | Group-level wikis help keep your information at a higher level and accessible to a broader set of people. A few examples of what you can put in your group wikis include team-specific information, coding style guides, and designs for your brand or your company.|
| [Assignee Boards/Lists](https://docs.gitlab.com/ee/user/project/issue_board.html) |  Streamline assignment of work to team members in a graphical assignment board.   |
| [Group Issue Boards](https://docs.gitlab.com/ee/user/project/issue_board.html#multiple-issue-boards) |  Visually manage programs (groups) with multiple issue boards where work can be dynamically assigned and tracked.  |
| [Issue Analytics](https://docs.gitlab.com/ee/user/group/issues_analytics/index.html) | Establish consistent and standard practices |
| [Weights](https://docs.gitlab.com/ee/user/project/issues/issue_weight.html)         | Prioritize and move forward what moves the needle  |
| [Multiple assignees](https://docs.gitlab.com/ee/user/project/issues/multiple_assignees_for_issues.html)     | Get collaboration going by letting the different profiles in your teams work on the same place concurrently |
| [Related issues](https://docs.gitlab.com/ee/user/project/issues/related_issues.html)      | Browse fast connected issues across groups and projects. Birds-eye view of all progress in the same direction |


#### Deploy with confidence

| Accelerate software delivery with integrated deployment and release management. | ![Multiple Project Pipeline Graphs](https://docs.gitlab.com/ee/ci/img/multi_project_pipeline_graph.png) |
| Maintain an end to end picture of how your applications are deployed and delivering business value.  | [![Operations Dashboard](https://docs.gitlab.com/ee/user/operations_dashboard/img/index_operations_dashboard_with_projects.png)](https://docs.gitlab.com/ee/user/operations_dashboard/img/index_operations_dashboard_with_projects.png) |

| **Features**    | **Value** |
| --------- | ------------ |
| [Operations Dashboard](https://docs.gitlab.com/ee/user/operations_dashboard/index.html#doc-nav) | a holistic view of the overall health of your company's operations.  |
| [Multi-project pipeline visualization](https://docs.gitlab.com/ee/ci/multi_project_pipelines.html#multi-project-pipeline-visualization) | In the Merge Request Widget, multi-project pipeline mini-graphs are displayed, and when hovering or tapping (on touchscreen devices) they will expand and be shown adjacent to each other.|
| [Merge Trains](https://docs.gitlab.com/ee/ci/merge_request_pipelines/pipelines_for_merged_results/merge_trains/) | Improve pipeline efficiency with merge concurrency to minimize merge wait times |
| [Browser Performance Testing](https://docs.gitlab.com/ee/user/project/merge_requests/browser_performance_testing.html) | Detect performance regressions for web apps before merging into master. |
| [Load Performance Testing](https://docs.gitlab.com/ee/user/project/merge_requests/load_performance_testing.html) | With Load Performance Testing, you can test the impact of any pending code changes to your application’s backend in GitLab CI/CD. |

#### Manage Compliance in the DevOps Process

| Simplify compliance with and traceability with enterprise features built into the developer's workflow. | ![Merge Request Reviews](https://docs.gitlab.com/ee/administration/img/auditor_access_form.png) |

| **Features**    | **Value** |
| --------- | ------------ |
| [Instance Events (Only Premium)](https://docs.gitlab.com/ee/administration/audit_events.html#instance-events) | Server-wide audit logging introduces the ability to observe user actions across the entire instance of your GitLab server, making it easy to understand who changed what and when for audit purposes.|
| [Auditor users](https://docs.gitlab.com/ee/administration/auditor_users.html) | Read-only access to all projects, groups, and other resources on the GitLab instance |
| [Verified Committer](https://docs.gitlab.com/ee/push_rules/push_rules.html#enabling-push-rules) | Ensure only authorized and verified team members are allowed to commit to the project   |
| [Require Signed Commits](https://docs.gitlab.com/ee/push_rules/push_rules.html#enabling-push-rules) | Enforce policy to require signed commits from contributors  |
| [IP Access Restriction](https://docs.gitlab.com/ee/user/group/#ip-access-restriction) |  Enable granular access controls to allow specific people access to specific resources like groups and their underlying projects by IP Address. |
| [Smart Card Authentication](https://docs.gitlab.com/ee/administration/auth/smartcard.html) | Simplify and streamline logon process to utilize authentication via smartcard   |

#### Dashboards and analytics

| [Operations Dashboard](https://docs.gitlab.com/ee/user/operations_dashboard/index.html#doc-nav) | a holistic view of the overall health of your company's operations.  |
| [Productivity Analytics](https://docs.gitlab.com/ee/user/analytics/productivity_analytics.html) | Analyze graphs and reports to understand team, project, and group productivity for uncovering patterns and best practices to improve overall productivity.   |
| [Contribution analytics](https://docs.gitlab.com/ee/user/group/contribution_analytics/) | Understand and assess the different operational gaps your projects may run into  |
| [Customizable Value Stream Analytics](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html#customizable-value-stream-analytics) | Create multiple value streams, hide default stages and create custom stages that align better with your development workflow |
| [Repository Analytics](https://docs.gitlab.com/ee/user/group/repositories_analytics/) | Visualize code coverage for select or all projects in your group |
| [Issue Analytics](https://docs.gitlab.com/ee/user/group/issues_analytics/index.html) | Establish consistent and standard practices |
| [Code Review Analytics](https://docs.gitlab.com/ee/user/analytics/code_review_analytics.html) | Find bottlenecks in your code review process |

#### Achieve High Availability and Disaster Recovery

| Achieve reliability and performance of your DevOps service through geographic replication and HA/DR solutions. | ![Geographic Replication](https://about.gitlab.com/images/gitlab_ee/gitlab_geo_diagram.png) |

| **Features**    | **Value** |
| --------- | ------------ |
| High Availability via our [Reference Architectures](https://about.gitlab.com/solutions/reference-architectures/) | Avoid downtime and outages, ensuring developers are able to work at all times.  |
| [Disaster Recovery](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/index.html) | Fail-over to another data center within minutes  |
| [Geographic Replication](https://about.gitlab.com/solutions/geo/) |  Reduce latency between distributed teams and increase developer productivity with globally distributed cloning and container registry geographic replication.  |
| [Maintenance Mode](https://docs.gitlab.com/ee/administration/maintenance_mode/index.html) |  Allows Systems administrators offer the highest level of access to their users while maintenance operations are in progress, with minimal disruption  |


#### Enterprise level Support

| **Features** |  **Value** |
| [Priority Support](https://about.gitlab.com/support/#priority-support) | Minimize outages and downtime with 4 hour response time for regular business support and 24x7 uptime support with a guaranteed 30 minute response time  |
| [Live Upgrade Assistance](https://about.gitlab.com/support/) | Schedule an upgrade time with GitLab. We’ll join a live screen share to help you through the process to ensure there aren't any surprises.  |
| [Technical Account Manager](https://about.gitlab.com/services/technical-account-management/) | The TAM will help guide, plan and shape the deployment and implementation of GitLab, and partner to help you get the best value possible out of your relationship with GitLab. _Available to Premium customers spending $50,000 per year or more on their license._ |

<center><a href="/sales/" class="btn cta-btn orange">Contact sales and learn more about GitLab Premium</a></center>
