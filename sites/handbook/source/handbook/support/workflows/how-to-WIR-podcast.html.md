---
layout: handbook-page-toc
title: How to do a WIR Podcast
category: References
description: General guide for creating a Support Week-in-Review Podcast
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

##### Overview

Use this workflow when you want to do a Week-in-Review Podcast as a general guide.

---

##### Workflow

1. Check the Support Team Google calendar for the recording session (near the end of the week).
1. Determine who will be responsible for each of the following roles. One person can be responsible for them all.
   - Editor: 
      - add theme music
      - ensure the audio levels are consistent across recordings
      - coalesce all of the audio files into the final mix using [Audacity](https://www.audacityteam.org/) (or something else)
      - publish the final mix onto Slack and link into the WIR document

   - Narrator(s):
      - analyze the content of the section you'll be narrating: click on each link and understand what is being expressed by the point
      - create a script that describes the content of each point in your section
      - read and record the content of your script
      - export the audio and provide it to the editor before the WIR discussion link is generated

   - Metrics analyst
      - review the current OKRs and formulate an analysis of any trends
      - take screenshots of the key metrics and paste them into the WIR document
      - in text, enter key metrics into the appropriate sections of the WIR document
      - write a script summarizing your findings
      - read and record the content of your script
      - export the audio and provide it to the editor before the WIR discussion link is generated


2. Execute the above before the discussion thread hits Slack (see the #support-team-chat Slack reminders to see when this gets posted).
