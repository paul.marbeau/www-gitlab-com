# www-gitlab-com

This is the source for the https://about.gitlab.com/ site. For a guide on how to start editing the website using git, see the [handbook page on that topic](https://about.gitlab.com/handbook/git-page-update).

## Local development

[The git-page-update handbook page](https://about.gitlab.com/handbook/git-page-update/#editing-the-handbook) is the single source of truth for how to get this repository set up for local development. 

We do, however, have additional details listed in the [doc/development.md](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/development.md) file. Regardless, you should start from the single source of truth for Ruby installation instructions and other dependencies.

## Netlify CMS

[Netlify CMS for developers](doc/netlifycms.md)

## Contributing

Please see the [contribution guidelines](CONTRIBUTING.md)

### Adding yourself to the team page

Please follow the instructions [in the handbook](https://about.gitlab.com/handbook/git-page-update/#11-add-yourself-to-the-team-page).

### Adding a pet to the team pets page

Edit [`data/pets.yml`](./data/pets.yml) and add a new entry.

Images should be uploaded to [`sites/uncategorized/source/images/team/pets`](./sites/uncategorized/source/images/team/pets).

### Blog posts

[Read how to add a blog post.](doc/blog-posts.md)

### Adding an application to the applications page (under `/applications`)

[How to add an application.](doc/applications.md)

### Updating the promotion link

This link appears at the top of the homepage and can be used to promote new
versions or upcoming events.

Edit [`data/promo.yml`](./data/promo.yml) to update the `link` and `text`
properties.

### Update the features page (under `/features`)

[How to update the features page.](doc/features.md#update-the-features-page-under-features)

### Create or update the comparison pages (under `/comparison`)

[How to update the comparison pages.](doc/features.md#create-or-update-the-comparison-pages-under-comparison)

### Update the releases page (under `/releases`)

[How to update the releases page.](doc/releases.md)

### Update the projects page (under `/handbook/engineering/projects`)

[How to update the projects page.](doc/projects.md)

### Press releases page

[How to add a press release page.](doc/press.md)

## PDF files

[How to work with PDFs.](doc/pdf.md)

## Production build

This section has moved to the [`doc/development.md` "Production Build" section](./doc/development.md#production-build)

## Custom Generators

This section has moved to the [`doc/development.md` "Custom Generators" section](./doc/development.md#custom-generators)

## Review Apps

Thanks to the [Review Apps], the `www-gitlab-com` project supports live reviewing
of any website changes with every merge request. When a branch is pushed and
the pipeline is successfully run, you can see a link pointing to the live
environment in your merge request. The URL will be of the following scheme:
`<branch-name>.about.gitlab.com`.  Note that if you have underscores in your
branch name, they will be replaced with dashes in the URL.

Beware that:

- To successfully deploy a Review App, the branch must exist in the
  `www-gitlab-com` repository. That means that branches from forks will not
  deploy a Review App (hence MRs from contributors). For that case, you should
  have at least Developer access to the `www-gitlab-com` project or
  `gitlab-com` group.
- The generation of the direction, wishlist, and releases pages are omitted
  in branches and is run only on master. This helps to shave off some time from
  the build process. That means you won't be able to preview these pages with
  Review Apps.

[review apps]: https://docs.gitlab.com/ee/ci/review_apps/

## Use Docker to render the website

If you don't have a proper environment available to run Middleman, you can use
Docker to provide one for you locally on your machine. To have this
working correctly, you need to have at least Git and Docker installed, and an
internet connection available.

> **Note**: This repository uses `git-lfs` extension for versioning large files. Before clone you need [to install](https://git-lfs.github.com/) it.

1. Clone the repository in a local folder
   
   ```sh
   git clone git@gitlab.com:gitlab-com/www-gitlab-com.git
   ```
>  **Note**: If your git clone is timing out, update the ServerAliveInterval in ~/.ssh/config to a larger number. For example, below is the content of the ~/.ssh/config file. <br> 
> ```
> Host *
>    ServerAliveInterval 1200
>    TCPKeepAlive yes 
>    IPQoS=throughput
>```

2. Create a Docker container - **NOTE: Change `sites/mysite` to either `sites/handbook` (works currently) or omit it for the rest of the site (which will soon move under `sites/uncategorized`)
   ```sh
   docker create --name middleman -v "$PWD/www-gitlab-com":/site -w /site -p 4567:4567 \
   -e LC_ALL=C.UTF-8 ruby:3.0 /bin/bash -c 'bundle install && cd sites/mysite && bundle exec middleman'
   ```

   > **Note**: `$PWD` prints the current working directory. If your docker container log
   > says `Could not locate Gemfile`, adjust the path string to match where you are running
   > `docker create`. For example, based on your current directory, your command may change
   > to something like this by removing `/www-gitlab-com` from the string:
   >
   > ```shell
   > user www-gitlab-com %** docker create --name middleman -v `"$PWD"`:/site -w /site -p 4567:4567 \
   > -e LC_ALL=C.UTF-8 ruby:3.0 /bin/bash -c 'bundle install && cd sites/mysite && bundle exec middleman'
   > ```


3. Start the container
   ```sh
   docker start middleman
   ```

4. Connect to http://localhost:4567

> **Note**: You won't be able to connect immediately. Middleman takes a few minutes to render the site. Run `docker ps -ls` to see if middleman is still running.

5. Change your original content as usual, and see the changes in the browser as soon as
you save the new version of the file (otherwise, just restart the container)

6. When you have finished, stop the container
   ```sh
   docker stop middleman
   ```

> **Note**: Subsequent runs will just require `docker start middleman`.

## Building a single file

If you only want Middleman to build a single file, you can do that via the `--glob` parameter.
Here are a few things to keep in mind:

* The glob parameter must match the **destination** file.

* The `--no-clean` option should be included or Middleman will wipe out
  files that do not match the glob parameter.

For example, here's how to rebuild the Contribute page. Note how
`source/company/culture/index.html.md.erb` is mapped to
`company/culture/contribute/index.html`:

```sh
bundle exec middleman build --glob={company/culture/contribute/index.html} --no-clean
```

For blog posts,
`source/blog/posts/2017-05-23-attributes-of-successful-development-teams.html.md`
maps to `blog/2017/05/23/attributes-of-successful-development-teams/index.html`:

```sh
bundle exec middleman build --glob={2017/05/23/attributes-of-successful-development-teams/index.html} --no-clean
```

## Monorepo Reorganization in Progress

This repository is currently in the process of reorganization as a monorepo. You can review the approval issue for this [here](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6213).

With this reorganization, there is a new directory called `sites/`. Sub projects of `www-gitlab-com` will be refactored to this directory. 


## Conclusion (please leave this at the bottom of the doc)

In case someone forgot the most important commands and is catting this file from the command line we end by listing them:

```sh
kill -kill `lsof -t -i tcp:4567`
bundle exec rake new_post
open -a "Google Chrome" http://localhost:4567
cd sites/mysite && bundle exec middleman
```

or to execute the last two commands, just run:

```sh
bin/run
```
